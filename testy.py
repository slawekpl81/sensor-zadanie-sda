import unittest

from main import Thermometer, PressureSensor


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sensor_1 = Thermometer('kitchen')
        self.assertEqual(sensor_1.log_average, 0)
        sensor_1.measurement = 20.11
        self.assertEqual(sensor_1.description, 'Result of a measurement: 20.1 +/-0.2 [°C]')
        sensor_1.measurement = 24
        self.assertEqual(sensor_1.description, 'Result of a measurement: 24.0 +/-0.2 [°C]')
        self.assertEqual(sensor_1.log_average, 22.055)
        self.assertEqual(sensor_1.fahrenheit, 'Result of a measurement: 75.20 +/-32.36 [°F]')
        sensor_2 = PressureSensor('outside')
        sensor_2.measurement = 998
        self.assertEqual(sensor_2.description, 'Result of a measurement: 998 +/-10 [hPa]')
        self.assertEqual(sensor_2.bar, 'Result of a measurement: 1.00 +/-0.01 [bar]')


if __name__ == '__main__':
    unittest.main()
