from typing import List


class Sensor:
    def __init__(self, location: str):
        self._measurement: float = 0
        self._precision: float = float('inf')
        self._unit: str = 'N/A'
        self._location: str = location
        self._log: List[float] = []

    @staticmethod
    def round_to_precision(precision: float) -> int:
        temp_list = str(precision).split('.')
        if len(temp_list) > 1:
            return len(temp_list[1])
        else:
            return 0

    @property
    def log_average(self) -> float:
        try:
            return sum(self._log) / len(self._log)
        except ZeroDivisionError:
            return 0

    @property
    def measurement(self) -> float:
        return self._measurement

    @measurement.setter
    def measurement(self, value: float):
        self._measurement = value
        self._log.append(value)

    @property
    def description(self) -> str:
        round_prec = self.round_to_precision(self._precision)
        return f'Result of a measurement: {self._measurement:.{round_prec}f} +/-{self._precision} [{self._unit}]'


class Thermometer(Sensor):
    def __init__(self, location: str):
        super().__init__(location)
        self._unit = '°C'
        self._precision: float = 0.2

    @property
    def fahrenheit(self) -> str:
        converter = lambda x: 9 / 5 * x + 32
        round_prec = self.round_to_precision(converter(self._precision))
        return f'Result of a measurement: {converter(self._measurement):.{round_prec}f} ' \
               f'+/-{converter(self._precision)} [°F]'


class PressureSensor(Sensor):
    def __init__(self, location):
        super().__init__(location)
        self._unit = 'hPa'
        self._precision = 10

    @property
    def bar(self) -> str:
        converter = lambda x: x / 1000
        round_prec = self.round_to_precision(converter(self._precision))
        return f'Result of a measurement: {converter(self._measurement):.{round_prec}f} ' \
               f'+/-{converter(self._precision)} [bar]'


if __name__ == '__main__':
    pass
